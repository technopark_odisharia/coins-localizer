# coding: utf-8

import cv2
import itertools
import os

import numpy as np

from collections import namedtuple
from operator import itemgetter


class Point(namedtuple('Point', 'x y')):
    def __init__(self, x, y):
        pass

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __str__(self):
        return '{} {}'.format(self.x, self.y)


class Circle:
    def __init__(self, x, y, r):
        self.center = Point(x, y)
        self.r = r

    def __contains__(self, item):
        if self == item:
            return False

        return self.center.x - self.r < item.center.x - item.r + 5 and\
               self.center.x + self.r > item.center.x + item.r - 5 and\
               self.center.y - self.r < item.center.y - item.r + 5 and\
               self.center.y + self.r > item.center.y + item.r - 5

    def __eq__(self, other):
        return self.center == other.center and self.r == other.r

    def __str__(self):
        return '{}, {}'.format(str(self.center), self.r)


def autoCanny(img, sigma=0.55):
    median = np.median(img)

    lower = int(max(0, (1.0 - sigma) * median))
    upper = int(min(255, (1.0 + sigma) * median))

    return cv2.Canny(img, 0, 255)


img_path = 'data/4.jpg'

img = cv2.imread(img_path)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

blur = cv2.GaussianBlur(gray, (3, 3), 0)

ac = autoCanny(blur)

cv2.imwrite('ac.jpg', ac)

circles = cv2.HoughCircles(ac, cv2.HOUGH_GRADIENT, 1, 90, param1=70, param2=26, minRadius=10, maxRadius=150)

if circles is None:
    print('mope')
    exit(0)

circles = np.uint16(np.around(circles))

circles = circles[0, :]

print(len(circles))

circles = [Circle(x, y, r) for x, y, r in circles]

sorted_circles = sorted(circles, key=lambda c: c.r)[::-1]

circles = list()

for circle in sorted_circles:
    nested = False

    for c in sorted_circles:
        if circle in c:
            nested = True
            break

    if not nested:
        circles.append(circle)

for c in circles:
    cv2.circle(img, c.center, c.r, (255, 0, 0), 3)

cv2.imwrite('found.jpg', img)

cv2.waitKey(0)
